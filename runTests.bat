@echo off
cd /d %~dp0

echo Performing project build
gradle build 

echo.
echo Running Smoke tests
gradle test -Psuite=Smoke 

echo.
echo Running Regression tests
gradle test -Psuite=Regression 

echo.
echo Tests completed
pause





















