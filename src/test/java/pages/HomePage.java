package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {
    By searchResults = By.cssSelector("#songResultsWrapper .song-item");
    @FindBy(css = "a .avatar")
    private WebElement avatar;
    @FindBy(css = "li a.songs")
    private WebElement allSongsList;
    @FindBy(css = "[data-testid = 'sound-bar-play']")
    private WebElement soundBarVisualizer;
    @FindBy(css = "[type='search']")
    private WebElement searchField;
    @FindBy(css = "[data-test='view-all-songs-btn']")
    private WebElement viewAllButton;
    @FindBy(css = ".success.show")
    private WebElement successNotification;
    @FindBy(css = "div.success.show")
    private WebElement notificationMsg;

    @FindBy(xpath = "//i[@class='fa fa-sign-out']")
    private WebElement buttonLogsout;


    public HomePage(WebDriver givenDriver) {
        super(givenDriver);
    }

    public void clickButtonLogout(){
        click(buttonLogsout);
    }

    public String getDeletedPlaylistMsg() {
        findElement(notificationMsg);
        return notificationMsg.getText();
    }

    public void clickFirstSearchResult() {
        List<WebElement> songResults = driver.findElements(searchResults);
        songResults.get(0).click();
    }

    public WebElement getSuccessBanner() {
        return findElement(successNotification);
    }

    public void search(String text) {
        findElement(searchField);
        searchField.click();
        searchField.clear();
        searchField.sendKeys(text);
    }

    public void viewAllSearchResults() {
        click(viewAllButton);
    }

    public boolean isSongPlaying() {
        return findElement(soundBarVisualizer).isDisplayed();
    }


    public boolean isLoggedIn() {
        return findElement(avatar).isDisplayed();
    }

    public void chooseAllSongsList() {
        click(allSongsList);
    }
}
