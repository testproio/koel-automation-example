package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class LoginPage extends BasePage {

    @FindBy(css = "input[type='email']")
    private WebElement emailField;
    @FindBy(css = "input[type='password']")
    private WebElement passwordField;
    @FindBy(css = "button[type='submit']")
    private WebElement submitBtn;
    public LoginPage(WebDriver givenDriver) {
        super(givenDriver);
    }

    public void login() {
        provideEmail("demo@class.com");
        providePassword("te$t$tudent");
        clickSubmit();
    }

    public boolean isLoggedOut() {
        return findElement(passwordField).isDisplayed();
    }

    public boolean isDisplayedButtonSubmitLogin() {
        return findElement(submitBtn).isDisplayed();
    }

    public String getTextEmailValidationMessage() {
        return findElement(emailField).getAttribute("validationMessage");
    }

    public boolean isEmailValidationMessagePresent(String messageError) {
        String validationMessage = findElement(emailField).getAttribute("validationMessage");
        return validationMessage.contains(messageError);
    }


    public LoginPage provideEmail(String email) {
        wait.until(ExpectedConditions.elementToBeClickable(emailField)).clear();
        emailField.sendKeys(email);
        return this;
    }

    public LoginPage providePassword(String password) {
        wait.until(ExpectedConditions.elementToBeClickable(passwordField)).clear();
        passwordField.sendKeys(password);
        return this;
    }

    public LoginPage clickSubmit() {
        wait.until(ExpectedConditions.elementToBeClickable(submitBtn)).click();
        return this;
    }

    public WebElement getSubmitLoginButton() {
        return findElement(submitBtn);
    }

}

