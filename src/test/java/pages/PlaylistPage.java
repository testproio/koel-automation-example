package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class PlaylistPage extends BasePage {

    @FindBy(css = ".playlist:nth-child(3)")
    private WebElement firstPlaylist;
    @FindBy(css = "[name='name']")
    private WebElement playlistNameField;
    @FindBy(css = "div.success.show")
    private WebElement renamePlaylistSuccessMsg;

    @FindBy(css = "[data-testid='sidebar-create-playlist-btn']")
    private WebElement buttonCreatePlaylist;

    @FindBy(css = "[data-testid='playlist-context-menu-create-simple']")
    private WebElement buttoncContextMenuCreate;

    @FindBy(xpath = "//input[@name='name']")
    private WebElement inputfieldPlaylist;

    @FindBy(css = ".btn-add-to")
    private WebElement addToBtn;

    @FindBy(css = ".btn-delete-playlist")
    private WebElement buttonDeletePlaylist;

    @FindBy(css = ".playlist .virtual-scroller .title")
    private WebElement songPlaylist;

    @FindBy(css = "section#playlistWrapper .text" )
    private WebElement emptyPlaylist;

    @FindBy(xpath = "//section[@id='playlists']//li[@class='playlist playlist']//a[contains(text(), $name)]")
    private WebElement newPlaylist;


    public PlaylistPage(WebDriver givenDriver) {
        super(givenDriver);
    }


    public void clickDeletePlaylistBtn() {
        click(buttonDeletePlaylist);
    }

    public void clickSongPlaylist() {
        click(songPlaylist);
    }

    public void clickNewPlaylist(String name) {

            String xpathExpression = String.format("//section[@id='playlists']//li[@class='playlist playlist']//a[contains(text(), '%s')]", name);

            WebElement dynamicElement = driver.findElement(By.xpath(xpathExpression));

            dynamicElement.click();
        }

    public boolean getTextEmptyPlaylist(String name) {
        return findElement(emptyPlaylist).getText().contains(name);
    }


    public PlaylistPage deleteSongFromPlaylist() {
        songPlaylist.click();
        actions.moveToElement(findElement(songPlaylist)).sendKeys(Keys.DELETE).perform();
        return this;
    }

    public void openPlaylist() {
        click(firstPlaylist);
    }

    public void createNewPlaylistWhileAddingSong(String playlistName){
        List<WebElement> nameField = driver.findElements(By.xpath("//section[@class='new-playlist']" +
                "//form[@class='form-save form-simple form-new-playlist']//input"));

        nameField.get(2).click();
        nameField.get(2).clear();
        nameField.get(2).sendKeys(playlistName);
        new Actions(driver)
                .keyDown(Keys.ENTER)
                .perform();
    }


    public void addSongToPlaylist(){
       addToBtn.click();
    }

        public PlaylistPage enterCreatePlaylist() {
            findElement(buttonCreatePlaylist);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buttonCreatePlaylist.click();
            return this;
        }

    public PlaylistPage enterContextMenuCreate() {
        buttoncContextMenuCreate.click();
        return this;
    }

    public String getNamePlaylist(String namePlaylist) {
        WebElement playlist = driver.findElement(By.xpath("//a[text()='" + namePlaylist + "']"));
        return playlist.getText();
    }

    public PlaylistPage doubleClickPlaylist() {
        findElement(firstPlaylist);
        actions.doubleClick(firstPlaylist).build().perform();
        return this;
    }

    public PlaylistPage clickPlaylist() {
        click(firstPlaylist);
        return this;
    }


    public PlaylistPage enterNewPlaylistName(String playlistName) {
        playlistNameField.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE));
        playlistNameField.sendKeys(playlistName);
        playlistNameField.sendKeys(Keys.ENTER);
        return this;
    }

    public String getRenamePlaylistSuccessMsg() {
        findElement(renamePlaylistSuccessMsg);
        return renamePlaylistSuccessMsg.getText();

    }

}