package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends BasePage {

    @FindBy(xpath = "//input[@name='q']")
    private WebElement inputField;

    @FindBy(xpath = "//section[@class='songs']//span[@class='details']")
    private WebElement songDetailsContainer;

    @FindBy(css = ".results .artists .name")
    private WebElement nameArtist;

    @FindBy(css = ".results .albums .name")
    private WebElement nameAlbum;

    public SearchPage(WebDriver givenDriver) {
        super(givenDriver);
    }

    public void enterSongName(String songName) {
        inputField.clear();
        inputField.click();
        findElement(inputField).sendKeys(songName);
    }

    public boolean verifySongFound(String songName) {
        findElement(songDetailsContainer);
       return songDetailsContainer.getText().contains(songName);

    }

    public String getNameArtist() {
        findElement(nameArtist);
        return nameArtist.getText();
    }

    public boolean getNameAlbum(String songName) {
        findElement(nameAlbum);
        return nameAlbum.getText().contains(songName);
    }
}
