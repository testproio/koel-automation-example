package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SongsPage extends BasePage {
    @FindBy(css = ".all-songs tr.song-item:nth-child(1)")
    private WebElement firstSong;

    @FindBy(css = "li.playback")
    private WebElement playOption;

    public SongsPage(WebDriver givenDriver) {
        super(givenDriver);
    }

    public void contextClickFirstSong() {
        actions.contextClick(findElement(firstSong)).perform();
    }

    public void choosePlayOption() {
        findElement(playOption).click();
    }

}
