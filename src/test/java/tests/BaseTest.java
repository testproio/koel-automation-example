package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.time.Duration;

public class BaseTest {

    private static final ThreadLocal<WebDriver> THREAD_LOCAL = new ThreadLocal<>();

    @Parameters({"baseURL"})
    @BeforeMethod
    public void setUp(@Optional String baseURL) {
        try {
            THREAD_LOCAL.set(pickBrowser(System.getProperty("browser")));
            getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            getDriver().get(baseURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to set up the browser: " + e.getMessage());
        }
        System.out.println(
                "Browser setup by Thread " + Thread.currentThread().getId() + " and Driver reference is : " + getDriver());
    }

    @AfterMethod
    public void tearDown() {
        if (getDriver() != null) {
            getDriver().quit();
            THREAD_LOCAL.remove();
        }
    }

    private WebDriver pickBrowser(String browserName) throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        String gridURL = "http://104.237.5.211:4444";
        browserName = browserName.toLowerCase();

        switch (browserName) {
            case "edge":
                capabilities.setCapability("browserName", "edge");
                break;

            case "firefox":
                capabilities.setCapability("browserName", "firefox");
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.addPreference("dom.webnotifications.enabled", false);
                firefoxOptions.addArguments("-fullscreen");
                firefoxOptions.addArguments("-lang=en");
                capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, firefoxOptions);
                break;

            case "chrome":
                capabilities.setCapability("browserName", "chrome");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--disable-notifications", "--remote-allow-origins=*", "--incognito", "--start-maximized");
                chromeOptions.addArguments("--lang=en");
                chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                break;
            default:
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--disable-notifications", "--remote-allow-origins=*", "--incognito", "--start-maximized");
                options.addArguments("--lang=en");
                options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
                return new ChromeDriver(options);
        }

        return new RemoteWebDriver(URI.create(gridURL).toURL(), capabilities);
    }

    public static WebDriver getDriver() {
        return THREAD_LOCAL.get();
    }

}
