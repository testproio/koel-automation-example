package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

public class LoginTests extends BaseTest {

    private static final String VALID_EMAIL = "demo@class.com";
    private static final String VALID_PASSWORD = "te$t$tudent";
    private static final String INVALID_PASSWORD = "1111111";

    @Test(description = "Successful login with valid credentials")
    public void testSuccessfulLogin() {
        LoginPage loginPage = new LoginPage(getDriver());
        HomePage homePage = new HomePage(getDriver());

        loginPage
                .provideEmail(VALID_EMAIL)
                .providePassword(VALID_PASSWORD)
                .clickSubmit();

        Assert.assertTrue(homePage.isLoggedIn(), "Login with valid credentials should be successful.");
    }

    @Test(description = "User logs out")
    public void testSuccessLogout() {
        LoginPage loginPage = new LoginPage(getDriver());
        HomePage homePage = new HomePage(getDriver());
        loginPage.login();
        homePage.clickButtonLogout();
        Assert.assertTrue(loginPage.isLoggedOut(), "User should be logged out.");
    }

    @Test(description = "Login with invalid email")
    public void testInvalidEmailLogin() {
        LoginPage loginPage = new LoginPage(getDriver());
        String actualErrorValidationMessage = loginPage.getTextEmailValidationMessage();
        String expectedErrorValidationMessage = "Please fill out this field.";

        loginPage
                .provideEmail("invalid_email")
                .providePassword(VALID_PASSWORD)
                .clickSubmit();

        Assert.assertEquals(actualErrorValidationMessage, expectedErrorValidationMessage,
                "Incorrect error message for invalid email login.");
    }

    @Test(description = "Login with invalid password")
    public void testInvalidPasswordLogin() {
        LoginPage loginPage = new LoginPage(getDriver());
        String actualErrorValidationMessage = loginPage.getTextEmailValidationMessage();
        String expectedErrorValidationMessage = "Please fill out this field.";

        loginPage
                .provideEmail(VALID_EMAIL)
                .providePassword(INVALID_PASSWORD)
                .clickSubmit();

        Assert.assertEquals(actualErrorValidationMessage, expectedErrorValidationMessage,
                "Incorrect error message for invalid password login.");
    }

    @Test(description = "Login with empty email")
    public void testEmptyEmailLogin() {
        LoginPage loginPage = new LoginPage(getDriver());
        String actualErrorValidationMessage = loginPage.getTextEmailValidationMessage();
        String expectedErrorValidationMessage = "Please fill out this field.";

        loginPage
                .provideEmail("")
                .providePassword(VALID_PASSWORD)
                .clickSubmit();

        Assert.assertEquals(actualErrorValidationMessage, expectedErrorValidationMessage,
                "Incorrect error message for empty email login.");
    }

    @Test(description = "Login with empty password")
    public void testEmptyPasswordLogin() {
        LoginPage loginPage = new LoginPage(getDriver());
        String actualErrorValidationMessage = loginPage.getTextEmailValidationMessage();
        String expectedErrorValidationMessage = "Please fill out this field.";

        loginPage
                .provideEmail(VALID_EMAIL)
                .providePassword("")
                .clickSubmit();

        Assert.assertEquals(actualErrorValidationMessage, expectedErrorValidationMessage,
                "Incorrect error message for empty password login.");
    }

    @Test(description = "Login with both empty email and password")
    public void testEmptyEmailPasswordLogin() {
        LoginPage loginPage = new LoginPage(getDriver());
        String actualErrorValidationMessage = loginPage.getTextEmailValidationMessage();
        String expectedErrorValidationMessage = "Please fill out this field.";

        loginPage
                .provideEmail("")
                .providePassword("")
                .clickSubmit();

        Assert.assertEquals(actualErrorValidationMessage, expectedErrorValidationMessage,
                "Incorrect error message for empty email and password login.");
    }

    @Test(description = "Login with invalid email credentials", dataProvider = "invalidEmails")
    public void testInvalidEmailLogin(String email, String expectedErrorValidationMessage) {
        LoginPage loginPage = new LoginPage(getDriver());

        loginPage
                .provideEmail(email)
                .providePassword(VALID_PASSWORD)
                .clickSubmit();

        String actualErrorValidationMessage = loginPage.getTextEmailValidationMessage();

        Assert.assertEquals(actualErrorValidationMessage, expectedErrorValidationMessage,
                "Incorrect error message for invalid email login: " + email);
    }

    @Test(description = "Login with additional invalid password credentials", dataProvider = "invalidPasswords")
    public void testInvalidPasswordLogin(String password, String description) {
        LoginPage loginPage = new LoginPage(getDriver());

        loginPage
                .provideEmail(VALID_EMAIL)
                .providePassword(password)
                .clickSubmit();

        Assert.assertTrue(loginPage.isDisplayedButtonSubmitLogin(), "Wrong password: " + description);
    }


    @DataProvider(name = "invalidEmails")
    private Object[][] provideInvalidEmails() {
        return new Object[][]{
                {"demo@@class.com", "A part following '@' should not contain the symbol '@'."},
                {"demo@class..com", "'.' is used at a wrong position in 'class..com'."},
                {"demo @class.com", "A part followed by '@' should not contain the symbol ' '."},
        };
    }

    @DataProvider(name = "invalidPasswords")
    private Object[][] provideInvalidPasswords() {
        return new Object[][]{
                {"te$t$tudent..", "Invalid password"},
                {"!@#$%^&*()", "Invalid password with special characters"},
                {"123456789012345678901234567890123456789012345678901234567890123456789012345678901", "Long password"},
                {"1", "Short password"},
        };
    }
}
