package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.PlaylistPage;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlaylistTests extends BaseTest {

    private String generatedPlaylistName;
    private final String newPlaylistName = generateUniquePlaylistName();

    @Test(enabled = true, priority = 1, description = "User creates a playlist")
    public void testCreatePlaylist() {
        LoginPage loginPage = new LoginPage(getDriver());
        PlaylistPage playlistPage = new PlaylistPage(getDriver());

        loginPage.login();
        playlistPage.enterCreatePlaylist();
        playlistPage.enterContextMenuCreate();

        generatedPlaylistName = generateUniquePlaylistName();
        playlistPage.enterNewPlaylistName(generatedPlaylistName);

        Assert.assertEquals(playlistPage.getNamePlaylist(generatedPlaylistName), generatedPlaylistName,
                "Playlist creation failed. The created playlist name is incorrect.");
    }

    @Test(enabled = true, priority = 2, description = "User renames the playlist")
    public void testRenamePlaylist() {
        LoginPage loginPage = new LoginPage(getDriver());
        PlaylistPage playlistPage = new PlaylistPage(getDriver());

        loginPage.login();

        String updatedPlaylistMsg = "Updated playlist \"" + newPlaylistName + ".\"";

        playlistPage.doubleClickPlaylist();
        playlistPage.enterNewPlaylistName(newPlaylistName);
        Assert.assertEquals(playlistPage.getRenamePlaylistSuccessMsg(), updatedPlaylistMsg,
                "Playlist renaming failed. The success message is incorrect.");
    }

    @Test(enabled = true, priority = 3, description = "User deletes the playlist")
    public void testDeletePlaylist() {
        String expectedPlaylistDeletedMessage = "Deleted playlist \"" + newPlaylistName + ".\"";
        LoginPage loginPage = new LoginPage(getDriver());
        PlaylistPage playlistPage = new PlaylistPage(getDriver());
        HomePage homePage = new HomePage(getDriver());

        loginPage.login();
        playlistPage.openPlaylist();
        playlistPage.clickDeletePlaylistBtn();

        Assert.assertEquals(homePage.getDeletedPlaylistMsg(), expectedPlaylistDeletedMessage,
                "Playlist deletion failed. The success message is incorrect.");
    }

    private final String generateUniquePlaylistName() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyHHmmss");
        String timestamp = dateFormat.format(new Date());

        return "PL" + timestamp;
    }
}
