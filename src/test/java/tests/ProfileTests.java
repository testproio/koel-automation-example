package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.ProfilePage;

public class ProfileTests extends BaseTest {

    @Test(enabled = true, description = "User changes the profile name")
    public void testChangeProfileName() {
        LoginPage loginPage = new LoginPage(getDriver());
        ProfilePage profilePage = new ProfilePage(getDriver());

        loginPage.login();
        profilePage.openProfile();

        profilePage.enterCurrentPassword("te$t$tudent");
        profilePage.enterCurrentEmail("demo@class.com");

        String newName = profilePage.generateRandomName();
        System.out.println("Generated Name: " + newName);
        profilePage.enterNewName(newName);
        profilePage.saveProfile();

        getDriver().navigate().refresh();
        String updatedName = profilePage.getProfileName();

        Assert.assertEquals(newName, updatedName, "Profile name is not updated correctly");
    }
}
