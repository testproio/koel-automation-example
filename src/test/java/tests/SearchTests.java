package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.SearchPage;

public class SearchTests extends BaseTest {

    @Test(enabled = true, description = "User searches for a song")
    public void testSearchForSong() {
        String songName = "Pluto";
        LoginPage loginPage = new LoginPage(getDriver());
        SearchPage searchPage = new SearchPage(getDriver());

        loginPage.login();
        searchPage.enterSongName(songName);

        Assert.assertTrue(searchPage.verifySongFound(songName), "The song '" + songName + "' is not found.");
    }

    @Test(enabled = true, description = "User searches for a song in Artist")
    public void testSearchForSongArtist() {
        String songName = "Dark";
        LoginPage loginPage = new LoginPage(getDriver());
        SearchPage searchPage = new SearchPage(getDriver());

        loginPage.login();
        searchPage.enterSongName(songName);

        Assert.assertEquals(searchPage.getNameArtist(), songName,"The song '" + songName + "' is not found.");
    }

    @Test(enabled = true, description = "User searches for a song in Album")
    public void testSearchForSongAlbums() {
        String songName = "Dark";
        LoginPage loginPage = new LoginPage(getDriver());
        SearchPage searchPage = new SearchPage(getDriver());

        loginPage.login();
        searchPage.enterSongName(songName);

        Assert.assertTrue(searchPage.getNameAlbum(songName), "The song '" + songName + "' is not found.");
    }
}
