package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.PlaylistPage;
import pages.SongsPage;

public class SongTests extends BaseTest {

    private final String namePlaylist = "Test Pro";

    @Test(enabled = true, description = "User plays a song")
    public void testPlaySong() {
        LoginPage loginPage = new LoginPage(getDriver());
        HomePage homePage = new HomePage(getDriver());
        SongsPage songsPage = new SongsPage(getDriver());

        loginPage.login();
        homePage.chooseAllSongsList();
        songsPage.contextClickFirstSong();
        songsPage.choosePlayOption();

        Assert.assertTrue(homePage.isSongPlaying(), "The song is not playing.");
    }

    @Test(enabled = true, priority = 1, description = "User adds a song to the playlist")
    public void testAddSongToPlaylist() {
        String song = "Waiting on a train";
        LoginPage loginPage = new LoginPage(getDriver());
        PlaylistPage playlistPage = new PlaylistPage(getDriver());
        HomePage homePage = new HomePage(getDriver());

        loginPage.login();

        homePage.search(song);
        homePage.viewAllSearchResults();
        homePage.clickFirstSearchResult();
        playlistPage.addSongToPlaylist();
        playlistPage.createNewPlaylistWhileAddingSong(namePlaylist);

        Assert.assertTrue(homePage.getSuccessBanner().isDisplayed(), "The song was not added to the playlist successfully.");
    }

    @Test(enabled = true, priority = 2, description = "User removes the song from the playlist")
    public void testRemoveSongFromPlaylist() {
        LoginPage loginPage = new LoginPage(getDriver());
        PlaylistPage playlistPage = new PlaylistPage(getDriver());
        String expectedResult = "The playlist is currently empty.";

        loginPage.login();
        playlistPage.clickPlaylist();
        playlistPage.clickNewPlaylist(namePlaylist);
        playlistPage.deleteSongFromPlaylist();

        Assert.assertTrue(playlistPage.getTextEmptyPlaylist(expectedResult), "The song was not removed from the playlist.");
        playlistPage.clickNewPlaylist(namePlaylist);
        playlistPage.clickDeletePlaylistBtn();
    }

 }
